import { extractUser, errorHandler, NotFoundError } from "@poonkt/common";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import logger from "./winston";

import { readAll } from "./routes/readAll";
import { readOne } from "./routes/readOne";
import { createOne } from "./routes/createOne";
import { updateOne } from "./routes/updateOne";
import { deleteOne } from "./routes/deleteOne";

const app = express();
app.set("trust proxy", true);
app.use(cors());	
app.use(morgan("combined", { stream: { write: (message) => logger.http(message) } }));
app.use(json());
app.use(
	cookieSession({
		signed: false,
		secure: false
	})
);
app.use(extractUser);

app.use(readAll);
app.use(readOne);
app.use(createOne);
app.use(updateOne);
app.use(deleteOne);

app.all("*", async () => {
	throw new NotFoundError();
});

app.use(errorHandler);

export { app };
