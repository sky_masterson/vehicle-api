import fs from "fs";
import { app } from "./app";
import logger from "./winston";

fs.readFile(__dirname + "/db/.initial-db.json", "utf-8", (err, data) => {
	if (!err) {
		fs.writeFile(__dirname + "/db/db.json", data, () => {
			logger.debug("Initial db clonned");

			app.listen(1337, () => {
				logger.info("API listening on port 1337!");
			});
		});
	}
});
