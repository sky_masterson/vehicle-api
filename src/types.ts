export interface Vehicle {
	id: number;
	year: number | string;
	brand: string;
	model: string;
	imageUrl: string;
	location: string;
	status: string;
	mileage: string | number;
	price: string | number;
}
