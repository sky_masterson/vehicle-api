import fs from "fs";
import path from "path";
import express, { Request, Response } from "express";
import { Vehicle } from "../types";

const router = express.Router();

type ResBody = Vehicle[];

router.get("/api/vehicles", async (_: Request, res: Response<ResBody>) => {
	fs.readFile(path.join(path.resolve("./") + "/src/db/db.json"), "utf-8", (err, data) => {
		if (!err) {
			res.status(200).send(JSON.parse(data));
		} else {
			res.status(500).send();
		}
	});
});

export { router as readAll };
