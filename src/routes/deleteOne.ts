import fs from "fs";
import path from "path";
import { param } from "express-validator";
import { validateRequest } from "@poonkt/common";
import express, { Request, Response } from "express";
import { Vehicle } from "../types";
import logger from "../winston";

const router = express.Router();

type Params = { id: number };

router.delete(
	"/api/vehicles/:id",
	param("id").notEmpty().isNumeric(),
	validateRequest,
	async (req: Request<Params>, res: Response<void>) => {
		const { params } = req;

		const dbPath = path.join(path.resolve("./") + "/src/db/db.json");

		fs.readFile(dbPath, "utf-8", (err, db) => {
			if (!err) {
				const data: Vehicle[] = JSON.parse(db);

				const foundIndex = data.findIndex((item) => item.id == params.id);

				if (foundIndex === -1) return res.status(404).send();

				data.splice(foundIndex, 1);

				fs.writeFile(dbPath, JSON.stringify(data), () => {
					logger.info("Existing listing deleted!");

					res.status(204).send();
				});
			} else {
				res.status(500);
			}
		});
	}
);

export { router as deleteOne };
