import fs from "fs";
import path from "path";
import { param } from "express-validator";
import express, { Request, Response } from "express";
import { Vehicle } from "../types";

const router = express.Router();

type Params = { id: number };
type ResBody = Vehicle[];

router.get(
	"/api/vehicles/:id",
	param("id").notEmpty().isNumeric(),
	async (req: Request<Params>, res: Response<ResBody>) => {
		const { params } = req;

		const dbPath = path.join(path.resolve("./") + "/src/db/db.json");

		fs.readFile(dbPath, "utf-8", (err, db) => {
			if (!err) {
				const data: Vehicle[] = JSON.parse(db);

				const found = data.find((item) => item.id == params.id);

				if (!found) return res.status(404).send();

				res.status(200).send([found]);
			} else {
				res.status(500);
			}
		});
	}
);

export { router as readOne };
