import fs from "fs";
import path from "path";
import { body, param } from "express-validator";
import { validateRequest } from "@poonkt/common";
import express, { Request, Response } from "express";
import { Vehicle } from "../types";
import logger from "../winston";

const router = express.Router();

type Params = { id: number };
type ReqBody = Vehicle;

router.patch(
	"/api/vehicles/:id",
	param("id").notEmpty().isNumeric(),
	body("year").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("brand").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("model").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("imageUrl").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("location").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("status").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("mileage").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("price").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	validateRequest,
	async (req: Request<Params, null, ReqBody>, res: Response<void>) => {
		const { body, params } = req;

		const dbPath = path.join(path.resolve("./") + "/src/db/db.json");

		fs.readFile(dbPath, "utf-8", (err, db) => {
			if (!err) {
				const data: Vehicle[] = JSON.parse(db);

				const foundIndex = data.findIndex((item) => item.id == params.id);

				if (foundIndex === -1) return res.status(404).send();

				const { id } = data[foundIndex];
				const merged = { ...body, id };

				data.splice(foundIndex, 1, merged);

				fs.writeFile(dbPath, JSON.stringify(data), () => {
					logger.info("Existing listing modified!");

					res.status(200).send();
				});
			} else {
				res.status(500);
			}
		});
	}
);

export { router as updateOne };
