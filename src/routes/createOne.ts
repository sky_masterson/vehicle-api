import fs from "fs";
import path from "path";
import { body } from "express-validator";
import { validateRequest } from "@poonkt/common";
import express, { Request, Response } from "express";
import { Vehicle } from "../types";
import logger from "../winston";

const router = express.Router();

type ReqBody = Vehicle;

router.post(
	"/api/vehicles",
	body("year").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("brand").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("model").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("imageUrl").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("location").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("status").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("mileage").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	body("price").notEmpty().withMessage("Shouldn't be empty").isString().withMessage("Must be a string"),
	validateRequest,
	async (req: Request<null, null, ReqBody>, res: Response<void>) => {
		const body = req.body;

		const dbPath = path.join(path.resolve("./") + "/src/db/db.json");

		fs.readFile(dbPath, "utf-8", (err, db) => {
			if (!err) {
				const data: Vehicle[] = JSON.parse(db);
				const id = Math.floor(Math.random() * 10000000000);
				data.push({ ...body, id });

				fs.writeFile(dbPath, JSON.stringify(data), () => {
					logger.info("New listing saved to DB");

					res.status(201).send();
				});
			} else {
				res.status(500);
			}
		});
	}
);

export { router as createOne };
